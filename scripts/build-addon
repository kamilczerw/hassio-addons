#!/bin/bash

DOCKER_REPO="kamilczerw"

if [ -f "images" ]; then
    rm images
fi

build_docker() {
    IFS=';' read -ra parts <<< "$1"
    addonDir=${parts[0]}
    dockerfile=${parts[1]}
    imageName=${parts[2]}

    cmd="docker build $addonDir/ -f $dockerfile -t $DOCKER_REPO/$imageName:$2"
    $cmd

    echo $DOCKER_REPO/$imageName:$2 >> images
}

IFS='-' read -ra parts <<< "$CI_COMMIT_TAG"
dir=${parts[0]}
version=${parts[1]}
if [ ! -d "$dir" ] || [ -z "$version" ]; then
    echo "Addon '$dir' does not exist or the version is not specified!"
    echo "Please check if you tag matches {addonName}-{version} format."
    echo "The {addonName} should match directory name."
    exit 1
fi

archs=$(cat "$dir/config.json" | jq --raw-output .arch[])

builds=()

if [ -z "$archs" ]; then
    builds[${#builds[@]}]="$dir;$dir/Dockerfile;$dir"
fi

for arch in $archs; do
    if [ ! -f "$dir/Dockerfile-$arch" ]; then
        echo "$dir/Dockerfile-$arch does not exists!"
        echo "Please add $dir/Dockerfile-$arch file or remove $arch from $dir/config.json, from arch section."
        exit 1
    fi
    builds+=("$dir;$dir/Dockerfile-$arch;$dir-$arch")
done

for build in ${builds[@]}; do 
    build_docker $build $version
done
